import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;

public class MainTest {
    private WebDriver driver;
    private final ChromeOptions options = new ChromeOptions();
    private WebDriverWait webDriverWait;
    private Actions hold;

    @BeforeMethod
    void setUp() {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        options.addArguments("incognito");
        options.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        driver = new ChromeDriver(options);
        webDriverWait = new WebDriverWait(driver, 30);
        hold = new Actions(driver);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    @AfterMethod
    void endTest() {
        driver.quit();
    }

    @Test
    void Task1() {
        driver.get("https://www.globalsqa.com/demo-site/draganddrop/");

        WebElement iframe = driver.findElement(By.xpath("//iframe[@class=\"demo-frame lazyloaded\"]"));
        driver.switchTo().frame(iframe);
        WebElement picture = driver.findElement(By.xpath("(//li[@class='ui-widget-content ui-corner-tr ui-draggable ui-draggable-handle'])[1]"));
        WebElement trashcan = driver.findElement(By.id("trash"));
        List<WebElement> trashcanBeforeMove = driver.findElements(By.xpath("//*[@id=\"trash\"]/ul/li/img"));
        webDriverWait.until(ExpectedConditions.visibilityOf(picture));
        hold.dragAndDrop(picture, trashcan).perform();
        hold.pause(2000).perform();
        List<WebElement> trashcanAfterMove = driver.findElements(By.xpath("//*[@id=\"trash\"]/ul/li/img"));
        Assert.assertTrue(trashcanBeforeMove.size() < trashcanAfterMove.size());

    }

    @Test
    void Task2() {
        driver.get("https://www.globalsqa.com/demo-site/select-dropdown-menu/");
        List<WebElement> countryListSize = driver.findElements(By.xpath("//*[@id=\"post-2646\"]/div[2]/div/div/div/p/select/option"));
        int expected = 249;
        assertEquals(countryListSize.size(), expected);
    }

    @Test
    void Task3() throws IOException {
        driver.get("https://www.globalsqa.com/samplepagetest/");
        WebElement input = driver.findElement(By.xpath("//input[@type=\"file\"]"));
        File profilePic = new File("src\\test\\resources\\73d39f5edee44dffbfc6480f9bfc.jpg");
        input.sendKeys(profilePic.getCanonicalPath());
        WebElement nameBox = driver.findElement(By.id("g2599-name"));
        nameBox.sendKeys("Javid");
        WebElement emailBox = driver.findElement(By.id("g2599-email"));
        emailBox.sendKeys("testmail@mailforspam.com");
        Select experinceYears = new Select(driver.findElement(By.id("g2599-experienceinyears")));
        experinceYears.selectByVisibleText("3-5");
        WebElement functionalCheckBox = driver.findElement(By.xpath("//input[@value=\"Functional Testing\"]"));
        functionalCheckBox.click();
        WebElement educationradioButton = driver.findElement(By.xpath("//input[@value=\"Post Graduate\"]"));
        educationradioButton.click();
        WebElement commentBox = driver.findElement(By.xpath("//textarea[@class=\"textarea\"]"));
        commentBox.sendKeys("Testing Task 3 for Homework.");
        WebElement submitButton = driver.findElement(By.xpath("//button[@type=\"submit\"]"));
        submitButton.click();
        List<WebElement> results = driver.findElements(By.xpath("//*[@id=\"contact-form-2599\"]/blockquote/p"));
        String expectedNameField = "Name: Javid";
        String expectedEmailField = "Email: testmail@mailforspam.com";
        String expectedWebsiteField = "Website:";
        String expectedExperienceField = "Experience (In Years): 3-5";
        String expectedExpertiseField = "Expertise :: Functional Testing";
        String expectedEducationField = "Education: Post Graduate";
        String expectedCommentField = "Comment: Testing Task 3 for Homework.";
        assertEquals(results.get(0).getText(), expectedNameField);
        assertEquals(results.get(1).getText(), expectedEmailField);
        assertEquals(results.get(2).getText(), expectedWebsiteField);
        assertEquals(results.get(3).getText(), expectedExperienceField);
        assertEquals(results.get(4).getText(), expectedExpertiseField);
        assertEquals(results.get(5).getText(), expectedEducationField);
        assertEquals(results.get(6).getText(), expectedCommentField);

    }

}